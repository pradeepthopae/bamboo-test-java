package org.tpgsi.bamboo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XrayBambooJunitTest {

    @Test
    public void testSayHello(){
        XrayBambooJunit xrayBambooJunit = new XrayBambooJunit();
        assertEquals("Hello World", xrayBambooJunit.sayHello());
    }
}
